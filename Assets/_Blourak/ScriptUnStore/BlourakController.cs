﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class BlourakController : MonoBehaviour {

    public BlourakSetAnimation blourakAnim;
    
    public float speedPower = 1f;
    public float walkMaxVelocity = 1f;
    public float runMaxVelocity = 3f;
    public float jumpPower = 1f;

    public bool isBlocked;

    public Vector3 direction;
    public bool hasGround;
    public bool isWalking;
    public bool isRunning;
    public bool askToJump;
    public bool isJumping;

    public Checker groundDetector;


    

    public void Update()
    {

        SetAnimationParams(isWalking, isRunning, hasGround,isBlocked);
        if (isBlocked) return;
        direction = GetInput();
        hasGround = HasGround();
        isWalking = IsWalking();
        isRunning = IsRunning();
        askToJump = AskJumping();

        CheckModelDirection();
        JumpProcess();
        MoveProcess();

        
    
    }   

    private void SetAnimationParams(bool isWalking, bool isRunning, bool hasGround, bool isBlocked)
    {
        if (blourakAnim == null) return;
        blourakAnim.IsWalking = isWalking;
        blourakAnim.IsRunning = isRunning;
        blourakAnim.HasGround = hasGround;
        blourakAnim.IsBlocked = isBlocked;
    }
    
    private void MoveProcess()
    {

        if (direction.x != 0f)
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.right * (speedPower * Time.deltaTime * direction.x), ForceMode2D.Impulse);

            if ((isRunning || isJumping) && Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x) > runMaxVelocity) 
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(
                    Mathf.Clamp(
                    GetComponent<Rigidbody2D>().velocity.x,-runMaxVelocity,runMaxVelocity)
                    ,GetComponent<Rigidbody2D>().velocity.y);
            }
            else if (!isRunning && Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x) > walkMaxVelocity) 
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(
                    Mathf.Clamp(
                    GetComponent<Rigidbody2D>().velocity.x, -walkMaxVelocity, walkMaxVelocity)
                    ,GetComponent<Rigidbody2D>().velocity.y);
            }
        }
    }

    private bool triggerJump;
    public void Jump(){
        triggerJump = true;
    }
    private void JumpProcess()
    {
        triggerJump = false;
        if (!isJumping && askToJump && hasGround)
        {
            ActivateAnimationJumpTrigger();
            isJumping = true;
            Vector2 rigVec = GetComponent<Rigidbody2D>().velocity;
            rigVec.y=0;
            GetComponent<Rigidbody2D>().velocity = rigVec;
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * jumpPower, ForceMode2D.Impulse);
        }
        if (isJumping && hasGround && GetComponent<Rigidbody2D>().velocity.y < 0f)
        {
            //onEndJump
            isJumping = false;
        }
    }

    private void ActivateAnimationJumpTrigger()
    {
        if (blourakAnim != null)
            blourakAnim.Jump();
    }

    private void CheckModelDirection()
    {
        if (direction.x > 0 && transform.localScale.x < 0f)
        {
            Vector3 newScale = transform.localScale;
            newScale.x = Mathf.Abs(newScale.x);
            transform.localScale = newScale;
            //OnSwitchDirection right
        }
        else if (direction.x < 0 && transform.localScale.x > 0f)
        {
            Vector3 newScale = transform.localScale;
            newScale.x = -Mathf.Abs(newScale.x);
            transform.localScale = newScale;
            //OnSwitchDirection left
        }
    }

   
    private bool IsWalking()
    {
        return (Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x) >= walkMaxVelocity *0.6f);
        
    }
    private bool IsRunning()
    {
        return GetComponent<Rigidbody2D>().velocity.x != 0f && !Input.GetKeyDown(KeyCode.LeftShift);// Mathf.Abs(Input.GetAxisRaw("Run")) == 0f;
    }
  

    private bool AskJumping()
    {
    #if UNITY_STANDALONE || UNITY_EDITOR
        if (groundDetector != null)
        return groundDetector.IsColliding2D() && (Input.GetButtonDown("Jump")|| triggerJump|| Input.GetKeyDown(KeyCode.UpArrow));
    #else
        if(groundDetector!=null)
        return groundDetector.IsColliding2D() && 
            (Input.GetMouseButtonDown(0)|| triggerJump);
#endif
        return false;
    }

    private bool HasGround()
    {
        if (groundDetector == null) return false;
        return groundDetector.IsColliding2D();
    }

    private Vector3 GetInput()
    {

        float horizontal = 0;
        float vertical = 0;
        #if UNITY_STANDALONE|| UNITY_EDITOR
            horizontal = Input.GetAxisRaw("Horizontal");
            vertical = Input.GetAxisRaw("Vertical");
        #else
                horizontal = Mathf.Clamp(Input.acceleration.x*2f,-1f,1f);
                vertical = Mathf.Clamp(Input.acceleration.y*2f,-1f,1f);
          
#endif
            return new Vector3(horizontal, vertical,0f);
        
    }



        public void SetAsBlocked(bool onOff)
        {
            isBlocked =onOff;
            GetComponent<Rigidbody2D>().isKinematic = onOff;
            if (onOff) 
            GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        }
}
