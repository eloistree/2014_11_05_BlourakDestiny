﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class MushRoomMovement : MonoBehaviour {
    public Animator animator;
    public float speed=1f;

    public Checker hasGroundPtCheck;
    public Checker hasWallPtCheck;

    public Vector2 direction = new Vector2(1f,0f);

    public delegate void OnDeath(MushRoomMovement enemy);
    public OnDeath onDeath;

	void Start () {
        onDeath += delegate(MushRoomMovement enemy){ Destroy(this.gameObject); };
	}
	
	void Update () {
        GetComponent<Rigidbody2D>().velocity = direction * speed;
        if (hasWallPtCheck.IsColliding2D() ) InverseDirection();
	
	}

    public void NotifyDeath ()
    {
        if (onDeath != null)
            onDeath(this);
    }

    public void InverseDirection() {
        Vector3 scale = transform.localScale;
        scale.x = -scale.x;
        transform.localScale=scale;
        direction.x = -direction.x;
    }
}
