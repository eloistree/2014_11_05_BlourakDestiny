﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof (LineRenderer))]
public class LineTrace : MonoBehaviour {

    
    public float distaneceBetweenDot=0.3f;
    
    public List<Vector3> points = new List<Vector3>();
    private Vector3 lastPointCreatedPosition;
    private LineRenderer lineRenderer;
	void Start () {
        lineRenderer = GetComponent<LineRenderer>() as LineRenderer;
        if (points.Count <= 1)
            points = new List<Vector3>() { new Vector3(0, 0, 0), new Vector3(0, 0, 0) };
       
        lineRenderer.SetVertexCount(points.Count);
	}

	
	void Update () {
        float d = Vector3.Distance(transform.position, lastPointCreatedPosition);

        points[0] = transform.position;
        lineRenderer.SetPosition(0, transform.position);
        if (d > distaneceBetweenDot) 
        {
            lastPointCreatedPosition = transform.position;
            for (int i = points.Count-1; i >0; --i)
            {
                print(i);
                points[i] = points[i-1];
                lineRenderer.SetPosition(i, points[i]);
            }
            
        }
        
        
	}

    
}
