﻿using UnityEngine;
using System.Collections;

public class ProjectileCollision : MonoBehaviour {

    public float dommage = 3f;
    public float heal = 3f;
    public MultiDimBelong dim;
    public GameObject destructionPrefab;
    public bool addRandomRGB=true;

    void Start()
    {
        dim.StopExisting(false);
        if (dim && addRandomRGB)
        {dim.AddRandomRGB();
        dim.AddRandomRGB();
        }
    }




    void OnCollisionEnter2D(Collision2D col) {

        if (col.gameObject.tag.Equals("Player"))
        {
            HasLife life = col.gameObject.GetComponent<HasLife>() as HasLife;
            if (life)
            {
                bool doDommage = ! BelongToRGB(dim);
                if (doDommage)
                    life.Life -= dommage;
                else life.Life += heal;
                //print("Player: " + life.Life);
            }
            AutoDestroy();
        }
        else if (col.gameObject.tag.Equals("LivingElement"))
        {
            MultiDimBelong dimElemt = col.gameObject.GetComponent<MultiDimBelong>() as MultiDimBelong;
            //print("Living element: " + dimElemt);
            dimElemt.AddDim(dimElemt);

            AutoDestroy();

        }
        else if (col.gameObject.tag.Equals("Boss")) 
        {
            if (BelongToRGB(dim))
            {
                HasLife life = col.gameObject.GetComponent<HasLife>() as HasLife;
                if (life)
                {
                    life.Life -= dommage;
                   // print("Boss: " + life.Life);
                    AutoDestroy();
                }
            }
        }
    
    }

    private bool BelongToRGB(MultiDimBelong dim)
    {
        if(!dim)return false;
        return dim.BelongTo(Dimensions.DimType.Red)
        && dim.BelongTo(Dimensions.DimType.Green)
        && dim.BelongTo(Dimensions.DimType.Blue);
    }
    public bool deactivateOnCollision=true;
    public bool destroyOnCollision=true;
    private void AutoDestroy()
    {
        dim.StopExisting(true);
        if(deactivateOnCollision)
        gameObject.SetActive(false);
        if(destroyOnCollision)
        Destroy(this.gameObject);
    }
}
