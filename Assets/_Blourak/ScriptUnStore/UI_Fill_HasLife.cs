﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI_Fill_HasLife : MonoBehaviour {

    public Image fillImage;
    public HasLife life;
	void Start () {

        if (!life) return;
        life.onLifeChange += LifeHasChange;
        LifeHasChange(life);

	}
    public void OnDestroy() {
        if (!life) return;
        life.onLifeChange -= LifeHasChange;

    }
    private void LifeHasChange(HasLife obj, float oldLife, float newLife)
    {
        LifeHasChange(obj);
    }

    private void LifeHasChange(HasLife obj)
    {
        if (!fillImage || !obj) return;
        fillImage.fillAmount = obj.GetPourcentCompareToInital();
    }
	
}
