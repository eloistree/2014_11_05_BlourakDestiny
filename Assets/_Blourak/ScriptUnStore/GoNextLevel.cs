﻿using UnityEngine;
using System.Collections;

public class GoNextLevel : MonoBehaviour {
    public string sceneName;

    public void Next() { Application.LoadLevel(sceneName); }
}
